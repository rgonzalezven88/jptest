from transaction import Transaction

class Portfolio(object):
    
    def __init__(self) -> None:
        self.__transactions = {}
        self.__value = {}
        self.__pnl = {}

    def getTransactions(self) -> dict:
        return self.__transactions

    def getTransactionByTicker(self, ticker: str):
        return self.__transactions.get(ticker)

    def buy(self, transaction: Transaction):
        if transaction.getTicker() in self.getTransactions():
            self.getTransactionByTicker(transaction.getTicker())['shares'] = self.getTransactionByTicker(transaction.getTicker())['shares'] + transaction.getQuantity()
            self.getTransactionByTicker(transaction.getTicker())['last_price'] = transaction.getPrice()
        else:
            self.__transactions[transaction.getTicker()] = { 
                'shares': transaction.getQuantity(),
                'type': transaction.getTransactionType(),
                'last_price': transaction.getPrice()
            }
    
    def sell(self, transaction: Transaction):
        if transaction.getTicker() not in self.getTransactions() or (transaction.getTicker() in self.getTransactions() and (self.getTransactions()[transaction.getTicker()]['shares'] < 0 or self.getTransactions()[transaction.getTicker()]['shares'] < transaction.getQuantity())):
            raise Exception("Can't sell")
        self.getTransactionByTicker(transaction.getTicker())['shares'] = self.getTransactionByTicker(transaction.getTicker())['shares'] - transaction.getQuantity()

    def generatePorfolioValue(self) -> None:
        for position, item in self.__transactions.items():
            self.__value[position] = item['shares'] * item['type'].getCurrentPrice()

    def generatePnL(self) -> None:
        for position, item in self.__transactions.items():
            self.__pnl[position] = item['shares'] * (item['type'].getCurrentPrice() - item['last_price'])

    def generateFromHistory(self, history: list) -> None:
        for transaction in history:
            if transaction.getOperation() == 'buy':
                self.buy(transaction)
            else:
                self.sell(transaction)