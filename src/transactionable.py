from abc import ABCMeta, abstractstaticmethod

class Transactionable(metaclass=ABCMeta):

    @abstractstaticmethod
    def getCurrentPrice():
        """A static inteface method"""