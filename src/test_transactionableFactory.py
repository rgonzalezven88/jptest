import unittest

from transactionableFactory import TransactionableFactory
from transactionable import Transactionable

class TestTransactionableFactory(unittest.TestCase):

    def test_create_transactionable(self):
        self.assertTrue(issubclass(TransactionableFactory.create('cash').__class__, Transactionable))

    def test_create_transactionable_fail(self):
        self.assertRaises(ValueError, TransactionableFactory.create, 'filtere')



        