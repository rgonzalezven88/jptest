from stock import Stock
from bond import Bond
from cash import Cash

class TransactionableFactory:
   
    @staticmethod
    def create(ttype: str):
        ttype = ttype.lower()
        if ttype == 'stock':
            return Stock()
        elif ttype == 'bond':
            return Bond()
        elif ttype == 'cash':
            return Cash()
        else:
            raise ValueError(type)