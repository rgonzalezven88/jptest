import unittest

from portfolio import Portfolio
from transaction import Transaction

class TestPortfolio(unittest.TestCase):

    def setUp(self):
        self.portfolio = Portfolio()

    def test_create_portfolio(self):
        self.assertIsInstance(self.portfolio, Portfolio)

    def test_buy(self):
        self.portfolio.buy(Transaction('TESTA', 198.1, 2, 'buy', 'stock', 'USD'))
        self.assertEqual(len(self.portfolio.getTransactions()), 1)

    def test_sell(self):
        self.portfolio.buy(Transaction('TESTA', 198.1, 4, 'buy', 'stock', 'USD'))
        self.portfolio.sell(Transaction('TESTA', 198.1, 2, 'buy', 'stock', 'USD'))
        self.assertEqual(len(self.portfolio.getTransactions()), 1)
        self.assertEqual(self.portfolio.getTransactionByTicker('TESTA')['shares'], 2)
        
    def test_sell_fail(self):
        self.portfolio.buy(Transaction('TESTA', 198.1, 4, 'buy', 'stock', 'USD'))
        self.assertRaises(Exception, self.portfolio.sell, Transaction('TESTA', 198.1, 5, 'buy', 'stock', 'USD'))

    def test_generate_from_history(self):
        portfolio = Portfolio()
        history = list()
        history.append(Transaction('A', 10, 1, 'buy', 'stock', 'USD'))
        history.append(Transaction('A', 10, 1, 'buy', 'stock', 'USD'))
        history.append(Transaction('A', 10, 1, 'sell', 'stock', 'USD'))
        history.append(Transaction('B', 20, 2, 'buy', 'cash', 'USD'))
        history.append(Transaction('C', 30, 3, 'buy', 'bond', 'USD'))
        portfolio.generateFromHistory(history)
        self.assertEqual(len(portfolio.getTransactions()), 3)
        