import unittest

from bond import Bond

class TestBond(unittest.TestCase):

    def setUp(self):
        self.bond = Bond()

    def test_create_instance(self):
        self.assertIsInstance(self.bond, Bond)
        