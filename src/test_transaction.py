import unittest

from transaction import Transaction
from transactionable import Transactionable

class TestTransaction(unittest.TestCase):

    def setUp(self):
        self.transaction = Transaction('TESTA', 198.1, 2, 'buy', 'stock', 'USD')

    def test_create_transaction(self):
        self.assertIsInstance(self.transaction, Transaction)

    def test_setter_and_getter_ticker(self):
        self.transaction.setTicker('NEWA')
        self.assertEqual(self.transaction.getTicker(), 'NEWA')

    def test_setter_and_getter_price(self):
        self.transaction.setPrice(1234.5)
        self.assertEqual(self.transaction.getPrice(), 1234.5)  

    def test_setter_price_fail(self):
        self.assertRaises(ValueError, self.transaction.setPrice, -12)
        self.assertRaises(ValueError, self.transaction.setPrice, 0)

    def test_setter_and_getter_quantity(self):
        self.transaction.setQuantity(10)
        self.assertEqual(self.transaction.getQuantity(), 10)

    def test_setter_quantity_fail(self):
        self.assertRaises(ValueError, self.transaction.setQuantity, -12)
        self.assertRaises(ValueError, self.transaction.setQuantity, 0)

    def test_setter_and_getter_operation(self):
        self.transaction.setOperation('sell')
        self.assertEqual(self.transaction.getOperation(), 'sell')
        self.transaction.setOperation('BuY')
        self.assertEqual(self.transaction.getOperation(), 'buy')

    def test_setter_operation_fail(self):
        self.assertRaises(ValueError, self.transaction.setOperation, 'TRANSF')

    def test_setter_and_getter_transactionType(self):
        self.transaction.setTransactionType('CaSh')
        self.assertTrue(issubclass(self.transaction.getTransactionType().__class__, Transactionable))

    def test_setter_transactionType_fail(self):
        self.assertRaises(ValueError, self.transaction.setTransactionType, 'filter')

    def test_setter_and_getter_currency(self):
        self.transaction.setCurrency('uSd')
        self.assertEqual(self.transaction.getCurrency(), 'USD')

    def test_setter_currency_fail(self):
        self.assertRaises(ValueError, self.transaction.setCurrency, 'ARG')