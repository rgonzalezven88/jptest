import unittest

from stock import Stock

class TestStock(unittest.TestCase):

    def setUp(self):
        self.stock = Stock()

    def test_create_instance(self):
        self.assertIsInstance(self.stock, Stock)
        