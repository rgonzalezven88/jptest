import unittest

from cash import Cash

class TestStock(unittest.TestCase):

    def setUp(self):
        self.cash = Cash()

    def test_create_instance(self):
        self.assertIsInstance(self.cash, Cash)
        