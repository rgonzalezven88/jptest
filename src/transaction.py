from transactionable import Transactionable
from transactionableFactory import TransactionableFactory

class Transaction(object):

    OPERATION_TYPES = ['buy', 'sell']
    CURRENCY_TYPES = ['USD', 'EUR']

    def __init__(self, ticker: str, price: float, quantity: int, operation: str, trasanctionType: str, currency: str):
        self.setTicker(ticker)
        self.setPrice(price)
        self.setQuantity(quantity)
        self.setOperation(operation)
        self.setTransactionType(trasanctionType)
        self.setCurrency(currency)

    def setTicker(self, ticker: str) -> None:
        self.__ticker = ticker

    def getTicker(self) -> str:
        return self.__ticker

    def setPrice(self, price: float) -> None:
        if price <= 0:
            raise ValueError('Incorrect Price')
        self.__price = price

    def getPrice(self) -> float:
        return self.__price

    def setQuantity(self, quantity: int) -> None:
        if quantity <= 0:
            raise ValueError('Incorrect Quantity')
        self.__quantity = quantity

    def getQuantity(self) -> float:
        return self.__quantity

    def setOperation(self, operation: str) -> None:
        if operation.lower() not in self.OPERATION_TYPES:
            raise ValueError('Invalid Operation')
        self.__operation = operation.lower()

    def getOperation(self) -> str:
        return self.__operation

    def setTransactionType(self, trasanctionType: str) -> None:
        self.__transactionType = TransactionableFactory.create(trasanctionType)

    def getTransactionType(self) -> Transactionable:
        return self.__transactionType

    def setCurrency(self, currency: str) -> None:
        if currency.upper() not in self.CURRENCY_TYPES:
            raise ValueError('Invalid Currency')
        self.__currency = currency.upper()

    def getCurrency(self) -> str:
        return self.__currency