from transactionable import Transactionable

class Cash(Transactionable):

    def getCurrentPrice(self) -> int:
        return 10 # Value only for test